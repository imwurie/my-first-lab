from django.shortcuts import render
from datetime import date

# Enter your name here
mhs_name = 'RA Wuriandita Wahyumurti Candra Kirana Dewi'
mhs_bornYear = 1997 # TODO Implement this

# Create your views here.
def index(request):
    response = {
    	'name': mhs_name,
    	'age': calculate_age(mhs_bornYear)}
    return render(request, 'index.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_year):
	today = date.today()
	year_now = today.year
	return year_now - birth_year
